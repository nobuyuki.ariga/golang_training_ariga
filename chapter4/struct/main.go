package main

import "fmt"

type Author struct {
	ID     int
	Name   string
	Gender string
}

type Book struct {
	ID    int
	Name  string
	Price int
	Author
}

func main() {
	author1 := Author{
		ID:     1,
		Name:   "Foo Bar",
		Gender: "Male",
	}

	book1 := Book{
		ID:     1,
		Price:  200,
		Name:   "Book_Hoge",
		Author: author1,
	}

	fmt.Println(book1, "exercise 2")

	p := &book1
	p.Name = "Book_Fuga"

	fmt.Println(book1, "exercise 3")
}
