package main

import "fmt"

func main() {
	array1 := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

	for _, val := range array1 {
		if val%2 == 0 {
			fmt.Println(val)
		}
	}
}
