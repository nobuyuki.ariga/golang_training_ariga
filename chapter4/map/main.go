package main

import (
	"fmt"
	"strings"
)

func main() {
	var input string
	fmt.Scan(&input)

	counterStrInputted := map[string]int{}
	for _, val := range strings.Split(input, "") {
		if _, ok := counterStrInputted[val]; ok {
			counterStrInputted[val] = counterStrInputted[val] + 1
		} else {
			counterStrInputted[val] = 1
		}
	}
	fmt.Println("result: ", counterStrInputted)
}
