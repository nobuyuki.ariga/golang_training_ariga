package main

import (
	"errors"
	"fmt"
)

type User struct {
	UserID int
	Name   string
}

type UserList struct {
	UserList []User
}

func (l *UserList) AddUser(u User) error {
	if u.UserID < 0 {
		return errors.New("UserID must be greater than 0")
	}
	for _, val := range l.UserList {
		if u.UserID == val.UserID {
			return errors.New("UserID existed")
		}
	}
	l.UserList = append(l.UserList, u)

	return nil
}

func main() {
	userList := new(UserList)

	err1 := userList.AddUser(User{UserID: 1, Name: "Hoge"})
	fmt.Println(err1)
	fmt.Println(userList.UserList)

	err2 := userList.AddUser(User{UserID: -2, Name: "Fuga"})
	fmt.Println(err2)
	fmt.Println(userList.UserList)

	err3 := userList.AddUser(User{UserID: 1, Name: "Piyo"})
	fmt.Println(err3)
	fmt.Println(userList.UserList)

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("recovered by r: ", r)
		}
	}()

	a := []int{1, 2}
	fmt.Println(a[9])
}
