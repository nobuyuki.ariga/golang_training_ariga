package main

import (
	"log"
	"net/http"
	"os"

	"github.com/ariga914/simpleapi/handler"
	"github.com/ariga914/simpleapi/middleware"
	"github.com/ariga914/simpleapi/models"
	"github.com/ariga914/simpleapi/services"
	"github.com/gorilla/mux"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func main() {
	//init mysql
	dsn := "root:123456@tcp(127.0.0.1:3306)/simpleapi"
	db, err := sqlx.Connect("mysql", dsn)

	if err != nil {
		log.Fatalln("Cannot connect to MySQL:", err)
	}
	log.Println("Connected: ", db)

	// Init router
	r := mux.NewRouter()

	//init model
	userModel := models.NewUserModel(db)

	//init service
	userService := services.NewUserService(userModel)

	//init handler
	helloHandler := handler.NewHelloHandler()
	userHandler := handler.NewUserHandler(userService)

	// Init Environment Variables, you can set anything for JWT_KEY
	err = os.Setenv("JWT_KEY", "my_secret_key")
	if err != nil {
		log.Fatalln("Cannot set Environment Variables:", err)
	}

	// Add router /helloworld with method GET
	r.HandleFunc("/helloworld", helloHandler.HelloWord).Methods("GET")

	// Add router /users/info with method GET to render user info
	r.HandleFunc("/users/info", userHandler.UserInfoHandler).Methods("GET")

	r.HandleFunc("/signup", userHandler.CreateUserHandler).Methods("POST")
	r.HandleFunc("/users/{id}", userHandler.UserInfo).Methods("GET")
	r.HandleFunc("/users", userHandler.UserList).Methods("GET")
	r.HandleFunc("/users/{id}", userHandler.UpdateUser).Methods("PUT")
	r.HandleFunc("/users/{id}", userHandler.DeleteUser).Methods("DELETE")

	//auth route
	r.HandleFunc("/login", userHandler.Login).Methods("POST")

	//init middleware
	r.Use(middleware.LoggingMiddleware)
	r.Use(middleware.AuthMiddleware)
	r.Use(middleware.SignUpMiddleware)

	// Create a server listening on port 8000
	httpPort := ":8000"
	log.Println("Starting http server on port", httpPort)
	s := &http.Server{
		Addr:    httpPort,
		Handler: r,
	}

	// Continue to process new requests until an error occurs
	log.Fatal(s.ListenAndServe())
}
