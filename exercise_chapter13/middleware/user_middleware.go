package middleware

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/ariga914/simpleapi/utils"
)

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		log.Println("logging request uri: ", r.RequestURI)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// If route signup, login we will skip it because these APIs does not need to check authenticated.
		if isSkipAuthMiddleWare(r) {
			next.ServeHTTP(w, r)
			return
		}

		// Get token from header
		token := r.Header.Get("Authorization")

		// Check token is valid
		_, ok := utils.ValidUserToken(token, os.Getenv("JWT_KEY"))
		if !ok {
			w.Header().Set("Content-Type", "application/json")
			// Response status is 401
			w.WriteHeader(http.StatusUnauthorized)
			resp := map[string]interface{}{}

			resp["error"] = "Unauthorized"
			json.NewEncoder(w).Encode(resp)
			return
		}

		// Call the next handler, which can be another middleware in the chain, or the final handler.
		next.ServeHTTP(w, r)
	})
}

func SignUpMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/signup" {
			log.Println("A user is being registered")
		}

		next.ServeHTTP(w, r)
	})
}

func isSkipAuthMiddleWare(r *http.Request) bool {
	skipList := map[string]bool{
		"/login":  true,
		"/signup": true,
	}
	if _, ok := skipList[r.URL.Path]; ok {
		return true
	}
	return false
}
