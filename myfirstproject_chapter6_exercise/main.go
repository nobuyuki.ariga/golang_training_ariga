package main

import (
	"fmt"
	"myfirstproject_chapter6_exercise/user"

	"rsc.io/quote"
)

func main() {
	fmt.Println(quote.Go())
	fmt.Println(quote.Glass())
	fmt.Println(quote.Hello())
	fmt.Println(quote.Opt())

	ul := user.UserList{}

	ul.AddUser(struct {
		ID   int
		Name string
	}{ID: 1, Name: "Hoge"})
	ul.Print()

	ul.AddUser(struct {
		ID   int
		Name string
	}{ID: 2, Name: "Fuga"})
	ul.Print()

	ul.AddUser(struct {
		ID   int
		Name string
	}{ID: 3, Name: "Piyo"})
	ul.Print()

}
