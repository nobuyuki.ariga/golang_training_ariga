package user

import (
	"fmt"
)

type privateUser struct {
	ID   int
	Name string
}

type UserList struct{ Users []privateUser }

func (ul *UserList) AddUser(u struct {
	ID   int
	Name string
}) {
	ul.Users = append(ul.Users, u)
}

func (ul *UserList) Print() {
	fmt.Println("length: ", len(ul.Users))
	fmt.Println("capacity: ", cap(ul.Users))
	for _, u := range ul.Users {
		fmt.Print(u)
	}
	fmt.Println("")
}
