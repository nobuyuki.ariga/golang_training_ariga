package main

import "fmt"

func MaxInt(s []int) int {
	if len(s) == 0 {
		return 0
	}

	max := s[0]
	for _, v := range s[1:] {
		if v > max {
			max = v
		}
	}
	return max
}

func MaxFloat64(s []float64) float64 {
	if len(s) == 0 {
		return 0
	}

	max := s[0]
	for _, v := range s[1:] {
		if v > max {
			max = v
		}
	}
	return max
}

func MaxNumber[T int | float64](s []T) T {
	if len(s) == 0 {
		return 0
	}

	max := s[0]
	for _, v := range s[1:] {
		if v > max {
			max = v
		}
	}
	return max
}

func main() {
	// Usage for int slice
	m1 := MaxNumber([]int{4, -8, 15})
	fmt.Println("Usage for int slice: ", m1)
	// Usage for float slice
	m2 := MaxNumber([]float64{4.1, -8.1, 15.1})
	fmt.Println("Usage for float slice: ", m2)
}
