package main

import (
	"fmt"

	"golang.org/x/tour/tree"
)

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, c chan int) {
	defer close(c)
	walk(t, c)
}

func walk(t *tree.Tree, c chan int) {
	if t == nil {
		return
	}
	walk(t.Left, c)
	c <- t.Value
	walk(t.Right, c)
}

func main() {
	c := make(chan int)
	go Walk(tree.New(1), c)
	for c := range c {
		fmt.Println(c)
	}
}
