package main

import (
	"fmt"
	"sync"
)

func main() {
	queue := make(chan int, 5)
	var wg sync.WaitGroup

	wg.Add(6)
	go func() {
		defer wg.Done()
		for a := 1; a <= 10000; a++ {
			fmt.Printf("URL %d has been enqueued\n", a)
			queue <- a
		}
		close(queue)
	}()

	for i := 0; i < 5; i++ {
		go Receive(queue, i, &wg)
	}

	wg.Wait()
}

func Receive(myChan <-chan int, workerName int, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		v, isAlive := <-myChan

		if !isAlive {
			break
		} else {
			fmt.Printf("Worker %d is crawling URL %d\n", workerName, v)
		}
	}
}
