package main

import "fmt"

type User struct {
	ID   int
	Name string
}

type UserList struct {
	Users []User
}

func (ul *UserList) Add(u User) {
	ul.Users = append(ul.Users, u)
}

func (ul *UserList) Remove(userTargeted User) {
	for idx, u := range ul.Users {
		if checker := u.ID == userTargeted.ID && u.Name == userTargeted.Name; checker {
			ul.Users = append(ul.Users[:idx], ul.Users[idx+1:]...)
			break
		}
	}
}

func (ul UserList) Print() {
	for _, u := range ul.Users {
		fmt.Println(u)
	}
}

func main() {
	// initialize user list struct
	userList := UserList{
		Users: make([]User, 0),
	}

	// add user struct to user list by Add method
	userList.Add(User{1, "AAA"})
	userList.Add(User{2, "BBB"})
	userList.Add(User{3, "CCC"})
	userList.Add(User{4, "DDD"})
	userList.Add(User{5, "EEE"})
	// print current user list by Print method
	userList.Print()
	// remove specified user struct from user list by Remove method
	userList.Remove(User{3, "CCC"})
	// print current user list by Print method
	userList.Print()
}
