package main

import "fmt"

//exercise 1
func filterOdd(numbers []int) []int {
	result := []int{}

	for _, val := range numbers {
		if val%2 != 0 {
			result = append(result, val)
		}
	}
	return result
}

//exercise 2
func printItems(input []int) {
	for idx, val := range input {
		fmt.Printf("a[%d] = %d\n", idx, val)
	}
}

//exercise 3
func fibonacci() func() int {
	prevBeforePrev := 0
	prev := 0
	counter := 0

	return func() int {
		switch counter {
		case 0:
			counter++
			return 0
		case 1:
			prev = 1
			counter++
			return 1
		default:
			sum := prev + prevBeforePrev
			prevBeforePrev = prev
			prev = sum
			return sum
		}
	}
}

func main() {
	//example for filterOdd call
	callResult := filterOdd([]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	fmt.Println(callResult)

	//example for printItems call
	printItems([]int{20, 100, 12, 8, 1, 7})

	//example for fibonacci call
	f := fibonacci()
	for i := 0; i < 20; i++ {
		fmt.Println(f())
	}
}
