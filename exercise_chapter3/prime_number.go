package main

import "fmt"

func main() {
	// find 1 digit prime number
	for i := 0; i < 100; i++ {
		counter := 0
		for j := 1; j < i; j++ {
			if i%j == 0 {
				counter++
			}
		}
		if counter == 1 {
			fmt.Printf("%d is Prime number\n", i)
		}
	}
}
