package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {

	counter := 0

	const num = 200
	var wg sync.WaitGroup
	wg.Add(num)

	// Declare mutex
	var mutex = &sync.Mutex{}

	for i := 0; i < num; i++ {
		go func() {
			//add statement to lock access to counter by other goroutinss
			mutex.Lock()

			temp := counter
			runtime.Gosched()
			temp++
			counter = temp
			//add statement in order to enable other goroutines to access locked counter
			mutex.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println("count:", counter)
}
