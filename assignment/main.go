package main

import "fmt"

// Node represents a node of linked list
type Node struct {
	value int
	next  *Node
}

// LinkedList represents a linked list
type LinkedList struct {
	head *Node
	len  int
}

func (ll *LinkedList) Insert(n *Node) {
	if ll.head == nil {
		ll.head = n
		ll.len = 1
		return
	}

	tail := ll.head
	for tail.next != nil {
		tail = tail.next
	}
	tail.next = n
	ll.len++
}

func (ll *LinkedList) Traverse() {
	current := ll.head
	if current == nil {
		return
	}

	for {
		fmt.Printf("linked list value: %d\n", current.value)

		if current.next != nil {
			current = current.next
		} else {
			break
		}
	}
}

func (ll *LinkedList) Remove(val int) {
	if ll.len == 0 {
		return
	}

	if ll.head.value == val {
		ll.head = ll.head.next
		return
	}

	prev := ll.head
	next := prev.next
	for prev.next != nil {
		if next.value == val {
			prev.next = next.next
			return
		} else {
			current := prev
			prev = current.next
			next = next.next
		}
	}
}

func main() {
	list := new(LinkedList)

	//test Remove() for empty list case
	list.Remove(1)
	list.Traverse()

	//test Insert() for empty list case
	list.Insert(&Node{value: 1})
	list.Traverse()
	fmt.Println("========================")

	//test Insert() for the case: "Insert after the tail"
	list.Insert(&Node{value: 2})
	list.Traverse()
	fmt.Println("========================")

	list.Insert(&Node{value: 3})
	list.Traverse()
	fmt.Println("========================")

	list.Insert(&Node{value: 3})
	list.Traverse()
	fmt.Println("========================")

	list.Insert(&Node{value: 4})
	list.Traverse()
	fmt.Println("========================")

	list.Insert(&Node{value: 5})
	list.Traverse()
	fmt.Println("========================")

	list.Insert(&Node{value: 6})
	list.Traverse()
	fmt.Println("========================")

	// test Remove() for the case: "If the value not found"
	list.Remove(7)
	list.Traverse()
	fmt.Println("========================")

	//test Remove() for the case: "Remove head node"
	list.Remove(1)
	list.Traverse()
	fmt.Println("========================")

	//test Remove() for the case: "Remove the first occurrence node"
	list.Remove(3)
	list.Traverse()
	fmt.Println("========================")

	//test Remove() for the case: "Remove tail node"
	list.Remove(6)
	list.Traverse()
	fmt.Println("========================")
}
