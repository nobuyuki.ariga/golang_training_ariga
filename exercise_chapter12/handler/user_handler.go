package handler

import (
	"encoding/json"
	"net/http"

	"github.com/ariga914/simpleapi_exercise/entities"
	"github.com/ariga914/simpleapi_exercise/services"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

// Create type for TodoItemHandler
type (
	TodoItemHandler struct {
		TodoItemService *services.TodoItemService
	}
)

// Init TodoItemHandler
func NewTodoItemHandler(TodoItemService *services.TodoItemService) *TodoItemHandler {
	return &TodoItemHandler{
		TodoItemService: TodoItemService,
	}
}

func (uh *TodoItemHandler) CreateTodoItemHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// Response status is 200
	w.WriteHeader(http.StatusOK)

	// decode request body
	reqBody := r.Body
	todoItem := &entities.TodoItem{}
	err := json.NewDecoder(reqBody).Decode(todoItem)

	//close the reader
	defer reqBody.Close()

	// define response
	resp := map[string]interface{}{}

	//Handle error; if there is an error with json decoding, return error
	if err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	//generates ID
	todoItem.ID = uuid.New().String()

	//call function to create from TodoItemService
	err = uh.TodoItemService.Create(todoItem)
	if err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	//returns todoItem information
	resp["todoItem"] = todoItem
	json.NewEncoder(w).Encode(resp)
}

func (u *TodoItemHandler) TodoItemInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// Response status is 200
	w.WriteHeader(http.StatusOK)

	// define response
	resp := map[string]interface{}{}

	// Get todoItemId from http request
	todoItemID := mux.Vars(r)["id"]

	// Get todoItem from service
	todoItem, err := u.TodoItemService.Info(todoItemID)
	if err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["data"] = todoItem
	json.NewEncoder(w).Encode(resp)
}

func (u *TodoItemHandler) TodoItemList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// Response status is 200
	w.WriteHeader(http.StatusOK)

	// define response
	resp := map[string]interface{}{}

	todoItems, err := u.TodoItemService.List()
	if err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["data"] = todoItems
	json.NewEncoder(w).Encode(resp)
}

func (u *TodoItemHandler) UpdateTodoItem(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// Response status is 200
	w.WriteHeader(http.StatusOK)

	// define response
	resp := map[string]interface{}{}

	// Decode request body into struct todoItem
	todoItem := &entities.TodoItem{}
	err := json.NewDecoder(r.Body).Decode(todoItem)
	if err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	// close the reader
	defer r.Body.Close()

	// Get todoItemId from http request
	todoItemID := mux.Vars(r)["id"]

	todoItem.ID = todoItemID
	err = u.TodoItemService.Update(todoItem)
	if err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["data"] = todoItem
	json.NewEncoder(w).Encode(resp)
}

func (u *TodoItemHandler) DeleteTodoItem(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// Response status is 200
	w.WriteHeader(http.StatusOK)

	// define response
	resp := map[string]interface{}{}

	// Get todoItemId from http request
	todoItemID := mux.Vars(r)["id"]

	if err := u.TodoItemService.Delete(todoItemID); err != nil {
		resp["error"] = err.Error()
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["data"] = ""
	json.NewEncoder(w).Encode(resp)
}
