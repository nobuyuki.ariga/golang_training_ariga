package main

import (
	"log"
	"net/http"

	"github.com/ariga914/simpleapi_exercise/handler"
	"github.com/ariga914/simpleapi_exercise/models"
	"github.com/ariga914/simpleapi_exercise/services"
	"github.com/gorilla/mux"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func main() {
	//init mysql
	dsn := "root:123456@tcp(127.0.0.1:3306)/simpleapi"
	db, err := sqlx.Connect("mysql", dsn)

	if err != nil {
		log.Fatalln("Cannot connect to MySQL:", err)
	}
	log.Println("Connected: ", db)

	// Init router
	r := mux.NewRouter()

	//init model
	todoItemModel := models.NewTodoItemModel(db)

	//init service
	todoItemService := services.NewTodoItemService(todoItemModel)

	//init handler
	todoItemHandler := handler.NewTodoItemHandler(todoItemService)

	r.HandleFunc("/todo_items", todoItemHandler.CreateTodoItemHandler).Methods("POST")
	r.HandleFunc("/todo_items/{id}", todoItemHandler.TodoItemInfo).Methods("GET")
	r.HandleFunc("/todo_items", todoItemHandler.TodoItemList).Methods("GET")
	r.HandleFunc("/todo_items/{id}", todoItemHandler.UpdateTodoItem).Methods("PUT")
	r.HandleFunc("/todo_items/{id}", todoItemHandler.DeleteTodoItem).Methods("DELETE")

	// Create a server listening on port 8000
	httpPort := ":8000"
	log.Println("Starting http server on port", httpPort)
	s := &http.Server{
		Addr:    httpPort,
		Handler: r,
	}

	// Continue to process new requests until an error occurs
	log.Fatal(s.ListenAndServe())
}
