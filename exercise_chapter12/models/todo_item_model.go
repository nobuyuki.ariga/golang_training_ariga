package models

import (
	"github.com/ariga914/simpleapi_exercise/entities"
	"github.com/jmoiron/sqlx"
)

// Create type for TodoItemModel
type (
	TodoItemModel struct {
		db *sqlx.DB
	}
)

func NewTodoItemModel(dbInstance *sqlx.DB) *TodoItemModel {
	return &TodoItemModel{
		db: dbInstance,
	}
}

func (um *TodoItemModel) CreateTodoItem(p *entities.TodoItem) error {
	sql := `INSERT INTO todo_items (id, todo) VALUES (?, ?)`
	_, err := um.db.Exec(sql, p.ID, p.TodoItem)
	return err
}

func (m *TodoItemModel) GetTodoItemByID(todoItemID string) (*entities.TodoItem, error) {
	sql := `SELECT * FROM todo_items WHERE id = ?`
	// New model
	todoItem := new(entities.TodoItem)

	// Execute query
	row := m.db.QueryRowx(sql, todoItemID)

	// Mapping db to model
	err := row.StructScan(todoItem)
	if err != nil {
		return nil, err
	}

	return todoItem, nil
}

func (m *TodoItemModel) GetTodoItemsList() ([]*entities.TodoItem, error) {
	sqlStr := `SELECT * FROM todo_items`
	var rows *sqlx.Rows
	var err error

	// Execute query
	rows, err = m.db.Queryx(sqlStr)
	if err != nil {
		return nil, err
	}

	todo_items := []*entities.TodoItem{}

	// Get list from query
	for rows.Next() {
		// init model
		var todoItem = &entities.TodoItem{}
		// Mapping db to model
		rows.StructScan(todoItem)
		todo_items = append(todo_items, todoItem)
	}

	return todo_items, nil
}

func (m *TodoItemModel) UpdateTodoItem(todoItem *entities.TodoItem) error {
	sql := `UPDATE todo_items SET todo =? WHERE id = ?`
	_, err := m.db.Exec(sql, todoItem.TodoItem, todoItem.ID)
	return err
}

func (m *TodoItemModel) DeleteTodoItem(todoItemID string) error {
	sql := `DELETE FROM todo_items WHERE id = ?`
	_, err := m.db.Exec(sql, todoItemID)
	return err
}
