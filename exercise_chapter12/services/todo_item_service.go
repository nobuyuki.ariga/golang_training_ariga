package services

import (
	"github.com/ariga914/simpleapi_exercise/entities"
	"github.com/ariga914/simpleapi_exercise/models"
)

// Create type for TodoItemServices
type (
	TodoItemService struct {
		todoItemModel *models.TodoItemModel
	}
)

func NewTodoItemService(todoItemModel *models.TodoItemModel) *TodoItemService {
	return &TodoItemService{
		todoItemModel: todoItemModel,
	}
}

func (u *TodoItemService) Create(todoItem *entities.TodoItem) error {
	// logic create todoItem to database, we will update in section working to database
	if err := u.todoItemModel.CreateTodoItem(todoItem); err != nil {
		return err
	}
	return nil
}

func (u *TodoItemService) Info(todoItemId string) (*entities.TodoItem, error) {
	todoItem, err := u.todoItemModel.GetTodoItemByID(todoItemId)
	if err != nil {
		return nil, err
	}

	return todoItem, nil
}

func (u *TodoItemService) List() ([]*entities.TodoItem, error) {
	todoItems, err := u.todoItemModel.GetTodoItemsList()
	if err != nil {
		return nil, err
	}

	return todoItems, nil
}

func (u *TodoItemService) Update(todoItem *entities.TodoItem) error {
	if err := u.todoItemModel.UpdateTodoItem(todoItem); err != nil {
		return err
	}

	return nil
}

func (u *TodoItemService) Delete(todoItemID string) error {
	if err := u.todoItemModel.DeleteTodoItem(todoItemID); err != nil {
		return err
	}

	return nil
}
